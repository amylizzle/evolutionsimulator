package NetworkComponents;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Hashtable;

import HelperFunctions.ConfigObject;
import SimComponents.AgentObject;

public class NetworkModuleThread extends Thread {
	private String centralhost = "www.ameliapollard.co.uk";
	private int centralport = 3456;
	private String staturl = "www.ameliapollard.co.uk/EvoSim/stats.php";
	private boolean sendStats = true;

	private Socket connection;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private int randomID;	
	private NetworkModuleSendThread sendThread;		
	private ArrayList<AgentObject> incomingAgents;	
	private int simWidth;
	private int simHeight;
	private volatile boolean ready = false;
	private volatile boolean died = false;
	private NetworkModuleStatThread statThread;

	public void initialise(int width, int height, int simID, ConfigObject conf)
	{
		String tmp = null;
		tmp = conf.getValue("host");
		if(tmp != null)
		{
			centralhost = tmp;
		}
		tmp = null;
		tmp = conf.getValue("port");
		if(tmp != null)
		{
			centralport = Integer.parseInt(tmp);
		}
		tmp = null;
		tmp = conf.getValue("statsurl");
		if(tmp != null)
		{
			staturl = tmp;
			sendStats = true;
		}
		else
		{
			sendStats = false;
		}


		//init variables
		this.simWidth = width;
		this.simHeight = height;
		this.randomID = simID;
		sendThread = new NetworkModuleSendThread();
		incomingAgents = new ArrayList<AgentObject>();
		died = false;
		statThread = new NetworkModuleStatThread();
	}

	public synchronized void killThread()
	{
		if(sendThread != null)
			sendThread.killThread();
		this.interrupt();
		this.died = true;
	}

	public boolean isDead()
	{
		return (this.isInterrupted() || this.died); 
	}

	public boolean isReady()
	{
		return this.ready;
	}

	public void run()
	{
		//this is in here because it blocks - can't be having a blocking call in the main sim thread
		//start connections
		try {		
			System.out.println(randomID+": Connecting to "+centralhost+" on "+centralport);
			connection = new Socket(centralhost,centralport);
			System.out.println(randomID+": Connected!");

			oos = new ObjectOutputStream(connection.getOutputStream());
			System.out.println(randomID+": Output stream initialised.");

			ois = new ObjectInputStream(connection.getInputStream());			
			System.out.println(randomID+": Input stream initialised.");

			sendThread.initialise(oos);
			System.out.println(randomID+": Send thread initialised");
			sendThread.start();
			System.out.println(randomID+": Send thread started!");

			System.out.println(randomID+": sending ID and details...");
			oos.writeInt(randomID);
			oos.writeInt(simWidth);
			oos.writeInt(simHeight);	
			oos.writeObject(new AgentObject()); //no idea why, but server won't read info from stream till an agent gets sent.
			//agents with 0 energy get dropped serverside, so this is okay to do, just not pretty.
			ready = true;  //set ready status
			System.out.println(randomID+": Finished! Network now ready for transport.");			
		} catch (Exception e) {
			System.out.println(randomID+": Network module failed to connect to host "+centralhost+" on port "+centralport+" ERROR:"+e);
			this.killThread();			
		}

		while(!this.isDead() )
		{
			AgentObject a;
			try {
				connection.setSoTimeout(1000);
				a = (AgentObject) ois.readObject();
				synchronized(incomingAgents)
				{
					incomingAgents.add(a);
				}				
			} 
			catch (SocketTimeoutException e)
			{
				//do nothing, just chillin'
			}
			catch (Exception e) {
				this.killThread();
				System.out.println(randomID+": Network module: Agent read failed! "+e);
				try { //try to shut things down cleanly
					oos.close();
					ois.close();
					connection.close();
				} catch (Exception e1) {
					//do nothing with errors, no point trying to recover stream
				}				
			}
		}
	}

	public void sendAgent(AgentObject a) 
	{
		if(sendThread != null)
		{
			if(sendThread.died)
			{
				System.out.println(randomID+": Agent send thread dead!");
				this.killThread();
			}
			else
			{	
				//using a concurrent linked queue to handle synchronization, so no need to synchronize here
				sendThread.agentsToSend.add(a);
			}
		}
	}

	public AgentObject[] getIncomingAgents()
	{		
		AgentObject[] result = new AgentObject[0];
		//not sure that synchronized blocks can ever be skipped, but don't wanna return a null if they can
		synchronized(incomingAgents)
		{
			result = incomingAgents.toArray(new AgentObject[0]);
			incomingAgents.clear();
		}
		return result;
	}

	public void sendStats(Hashtable<Integer,Integer> popTable, Integer simID)
	{
		if(sendStats)
		{
			if(!statThread.isAlive())
			{
				statThread = new NetworkModuleStatThread();
				statThread.initialise(popTable, staturl, simID);
				statThread.start();
			}
		}
	}
}
