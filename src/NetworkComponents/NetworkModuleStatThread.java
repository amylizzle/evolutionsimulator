package NetworkComponents;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;

public class NetworkModuleStatThread extends Thread {
	private Hashtable<Integer,Integer> popTable;
	private String posturl;
	private Integer simID = 0;
	
	public void initialise(Hashtable<Integer,Integer> popTable, String url, Integer simID)
	{
		this.popTable = popTable;
		this.posturl = url;
		this.simID = simID;
	}

	public void run()
	{
		try
		{
			String urlParameters = "simid="+this.simID+"&";
			Integer[] genera = popTable.keySet().toArray(new Integer[0]);
			for(int i=0; i<genera.length; i++)
			{
				urlParameters += genera[i]+"="+popTable.get(genera[i])+"&";
			}
			//System.out.println("Sending: "+urlParameters+" to "+posturl);
			URL url = new URL(this.posturl); 
			URLConnection conn = url.openConnection();

			conn.setDoOutput(true);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(urlParameters);
			writer.flush();
			
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while(reader.readLine() != null){};			
			reader.close(); // need to read for it to work. Stupid java.
			writer.close();      
			System.out.println("Sent stats to "+posturl+"!");
			return;
		}
		catch(Exception e)
		{
			System.out.println("An error occurred sending the stats! "+e.getMessage());
		}
	}
}
