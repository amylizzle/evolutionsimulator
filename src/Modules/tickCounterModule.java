package Modules;
import SimComponents.*; 
public class tickCounterModule extends Module {

		@Override		
		public void simEnd(SimObject s)
		{
			long seconds = s.getRunTime();
			long minutes = seconds / 60;
			seconds = seconds % 60;
			long hours = minutes / 60;
			minutes = minutes % 60;
			
			System.out.println(s.ID+": Runtime: "+hours+"h:"+minutes+"m:"+seconds+"s");
			System.out.println(s.ID+": Population: "+s.agents.size());
			System.out.println(s.ID+": Ticks: "+s.getTicks());
			System.out.println(s.ID+": Ticks per second: "+s.getTicksPerSecond());
			System.out.println(s.ID+": Average ticks per second: "+s.getAvgTicksPerSecond());
		}
}
