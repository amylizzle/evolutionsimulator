package Modules;
import java.util.Random;

import HelperFunctions.ConfigObject;
import SimComponents.*;


public class populateModule extends Module {

	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Populate module disabled!");
			return;
		}


		int popcount = 3000; 
		if(conf.getValue("count") != null)
		{
			popcount = Integer.parseInt(conf.getValue("count"));
		}
		int minDNA = 50;
		int maxDNA = 50;
		if(conf.getValue("mindnalength") != null)
		{
			minDNA = Integer.parseInt(conf.getValue("mindnalength"));
		}
		if(conf.getValue("maxdnalength") != null)
		{
			maxDNA = Integer.parseInt(conf.getValue("maxdnalength"));
		}
		System.out.println(s.ID+": Populating with "+popcount+" random agents");
		Random myrandom = new Random();  

		for(int i=0; i<popcount; i++)
		{			
			String randdna = "";
			int randsize = myrandom.nextInt((maxDNA-minDNA))+minDNA; //ensures every agent has at least 50 DNA, 
			for(int j=0; j<randsize; j++)
			{
				randdna += (char)(myrandom.nextInt(256));
			}
			s.agents.add(new AgentObject(myrandom.nextInt(s.width),myrandom.nextInt(s.height),randdna,myrandom.nextInt(10000),myrandom.nextInt(), myrandom.nextDouble()));			
		}



	}


}
