package Modules;

import HelperFunctions.ConfigObject;
import NaivePhysicsComponents.NaivePhysicsHandler;
import SimComponents.Module;
import SimComponents.SimObject;

public class physicsModule extends Module {

	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Physics module disabled!");
			return;
		}
		s.physics = new NaivePhysicsHandler();
	}

}
