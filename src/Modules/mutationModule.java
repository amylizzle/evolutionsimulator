package Modules;

import java.awt.Color;
import java.util.Random;

import HelperFunctions.ConfigObject;
import SimComponents.AgentObject;
import SimComponents.Module;
import SimComponents.SimObject;

public class mutationModule extends Module {

	ConfigObject conf;
	
	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.conf = conf;
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Mutation module disabled!");
			return;
		}
	}
	
	@Override
	public void agentTick(SimObject s, AgentObject a)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
			return;
		
		Random myrandom = new Random();
		int mutations = 0;

		if(a.mutationrate <= Double.MIN_NORMAL*200)
		{
			a.mutationrate = Double.MIN_NORMAL*200;
		}
		
		for(int mutpos=0; mutpos<a.dna.length(); mutpos++)
		{
			double randnum = myrandom.nextDouble();
			double mutationchance = s.mutationrate; //environmental mutation chance a la real-life
			if(randnum < mutationchance) //do a mutation
			{
				//chance to repair
				//roll a save!
				if(myrandom.nextDouble() > a.mutationrate)
				{
					//repair is going to happen instead of mutation, apply cost.
					a.energy--;
				}
				else //didn't repair - so mutate
				{
					randnum = myrandom.nextDouble(); //get a new random number
					mutations++;
					//System.out.println("RN: "+randnum); //debug
					if(randnum >= (2.0/6.0))  //mutate a random piece with 4/6 chance of mutation rate
					{
						if(a.dna.length()>0)
						{
							//select a random point in the DNA
							//	int mutpos = myrandom.nextInt(a.dna.length()+1);
							//split the DNA at point
							String tempdnastart = a.dna.substring(0,mutpos);
							//split the DNA at point +1 to skip a char
							String tempdnaend = "";
							if((mutpos+1) < a.dna.length())
							{
								tempdnaend = a.dna.substring(mutpos+1,a.dna.length());
							}
							//generate random new piece
							char randchar = (char)(myrandom.nextInt(256));  
							//and stick it all back together
							a.dna = tempdnastart + randchar + tempdnaend;
						}
					}
					else if(randnum >= (0.9999/6.0)) //insert at mutpos with ~1/6 chance of mutation rate
					{
						//select a random point in the DNA
						//	int mutpos = myrandom.nextInt(a.dna.length()+1);
						//split DNA at point
						String tempdnastart = a.dna.substring(0,mutpos);
						String tempdnaend = "";
						if(mutpos < a.dna.length())
						{
							tempdnaend = a.dna.substring(mutpos,a.dna.length());
						}
						//generate new piece
						char randchar = (char)(myrandom.nextInt(256));  
						//stick it all back together;
						a.dna = tempdnastart + randchar + tempdnaend;				
					}
					else// if(randnum >= (0.0/6.0)) //delete at mutpos with ~1/6 chance of mutation rate
					{
						if(a.dna.length()>1)
						{
							//select a random point in the DNA
							//	int mutpos = myrandom.nextInt(a.dna.length()+1);
							//split DNA at point
							String tempdnastart = a.dna.substring(0,mutpos);
							//split DNA at point + 1 to skip one character
							String tempdnaend = "";
							if((mutpos+1) < a.dna.length())
							{
								tempdnaend = a.dna.substring(mutpos+1,a.dna.length());
							}
							//stick back together
							a.dna = tempdnastart + tempdnaend;
						}
						else
						{
							a.dna = "";
						}

					}
					

					double change = (myrandom.nextDouble()*(0.01)) - (0.01/2.0);
					a.mutationrate += change;
					//System.out.println("Mutation change: "+change+" result: "+a.mutationrate);
				} //if not repair
			}// if did a mutation (randnum < mutationchance)
		}//for each DNA byte

		Color last = new Color(a.genus);
		float[] rgb = new float[3];
		last.getRGBColorComponents(rgb);
		float[] hsb = new float[3];
		Color.RGBtoHSB((int)(rgb[0]*256),(int)(rgb[1]*256),(int)(rgb[2]*256),hsb);
		//do maths to hsb
		double weightPerMutation = 10.0;
		if(a.dna.length() > 0) { weightPerMutation /= a.dna.length(); }
		double magnitude = weightPerMutation * mutations; //M
		magnitude *= magnitude; //M^2
		double modh = ((myrandom.nextDouble()*(magnitude/3.0))-((magnitude/1.5))); 		
		double mods = ((myrandom.nextDouble()*(magnitude/3.0))-((magnitude/1.4)));
		double modb = ((myrandom.nextDouble()*(magnitude/3.0))-((magnitude/1.4))); //half the colour space, half the magnitude?
		
		//I didn't say it was good maths... 
		//The general gist is that the change is always a distance of magnitude or less from where it was in HSB space 
		
		
		
		//hsb[0] is rotation, can wrap around
		hsb[0] += modh;
		
		//if mod takes outside of bounds, flip sign of mod to keep same magnitude but within bounds
		if(hsb[1] + mods < 0.4) { mods = mods * -1.0; }
		if(hsb[1] + mods > 1.0) { mods = mods * -1.0; }
		hsb[1] += mods;
		if(hsb[2] + modb < 0.4) { modb = modb * -1.0; }
		if(hsb[2] + modb > 1.0) { modb = modb * -1.0; }		
		hsb[2] += modb;
		
	//	if(mutations > 0)
	//		System.out.println(magnitude+" = "+(modh*modh)+" + "+(mods*mods)+" + "+(modb*modb));
		
		
		a.genus = Color.HSBtoRGB(hsb[0], hsb[1], hsb[2]); 	
	}

}
