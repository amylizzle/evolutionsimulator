package Modules;
import HelperFunctions.ConfigObject;
import HelperFunctions.ImagePanel;
import SimComponents.*; 

import java.util.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
public class superGuiModule extends Module implements MouseMotionListener {
	JFrame window;
	BufferedImage image;
	BufferedImage popGraph;
	Hashtable<Color,Integer> populationMappings;
	int highestPop = 0;
	long totalEnergy = 0;
	Color highestColor = Color.white;
	int popGraphHeight = 100;
	private AgentObject mouseOverAgent = new AgentObject();
	private Color mouseOverColor = new Color(0);

	private int mouseMode = 0;
	final int MOUSE_MODE_SAMPLE = 0;
	final int MOUSE_MODE_RANDOM = 1;
	final int MOUSE_MODE_DRAW = 2;


	private SimObject parentSim;

	//components
	private JPanel mainPanel;
	private JPanel sidePanel;
	private ImagePanel simPanel;
	private ImagePanel popPanel;
	private JPanel labelPanel;
	private JLabel populationLabel;
	private JLabel tickLabel;
	private JLabel ticksPerSecondLabel;
	private JLabel totalEnergyLabel;
	private JLabel generaLabel;
	private JLabel speciesLabel;
	private JLabel highestPopLabel;
	private JScrollPane viewPanelSP;
	private JPanel viewPanel;
	private JTextArea agentLabel;
	private JPanel buttonPanel;
	private JButton modeBtn;
	private JSlider radSlider;
	private JLabel radLabel;
	private JButton makeAgentBtn;

	boolean saveToFile = false; //saves the image of the sim every 50 ticks
	//so I could use ffmpeg to make it into a movie for Little Chip
	//FUTURE: make this a command line switch/config file?
	ConfigObject conf;
	

	
	private void saveImage(BufferedImage bi,long frameNumber)
	{
		try {
			// retrieve image
			File outputfile = new File("image"+frameNumber+".png");
			ImageIO.write(bi, "png", outputfile); //jpegs have too many artifacts for pixel level resolution, 
			//bitmaps are too damn big.
		} catch (IOException e) {
			System.out.println("Failed to save image!");
		}
	}
	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.conf = conf;
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": SuperGUI module disabled!");
			return;
		}
		
		if(conf.getValue("saveframes")!=null)
		{
			saveToFile=true;
		}
		
		this.parentSim = s;
		populationMappings = new Hashtable<Color,Integer>();
		System.out.println(s.ID+": Creating Window...");

		window = new JFrame("Simulation Viewer - "+s.ID);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			System.out.println("Error setting native LAF: " + e);
		}
		window.setVisible(true);
		window.setSize(s.width+450, s.height+200);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //does not close sim properly

		image = new BufferedImage(s.width,s.height,BufferedImage.TYPE_INT_RGB);
		popGraph = new BufferedImage(400,popGraphHeight,BufferedImage.TYPE_INT_RGB);
		
		
		mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(new Insets(20, 20, 20, 20)));
		sidePanel = new JPanel();
		sidePanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));

		simPanel = new ImagePanel(image);		
		simPanel.setPreferredSize(new Dimension(image.getWidth(),image.getHeight()));
		simPanel.addMouseMotionListener(this);
		simPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));	

		popPanel = new ImagePanel(popGraph);
		popPanel.setPreferredSize(new Dimension(popGraph.getWidth(),popGraph.getHeight()));


		labelPanel = new JPanel();
		labelPanel.setLayout(new GridLayout(8,1));
		labelPanel.setBorder(new EmptyBorder(new Insets(20, 20, 20, 20)));
		populationLabel = new JLabel("Population: 0");
		tickLabel = new JLabel("Ticks: 0");
		ticksPerSecondLabel = new JLabel("Ticks per second: 0");
		totalEnergyLabel = new JLabel("Total energy: 0");
		generaLabel  = new JLabel("Genera: 0");
		speciesLabel = new JLabel("Species: 0");
		highestPopLabel = new JLabel("Highest Population: 0");
		labelPanel.add(tickLabel);
		labelPanel.add(ticksPerSecondLabel);
		labelPanel.add(populationLabel);
		labelPanel.add(totalEnergyLabel);
		labelPanel.add(generaLabel);
		labelPanel.add(speciesLabel);
		labelPanel.add(highestPopLabel);

		viewPanel = new JPanel();
		agentLabel = new JTextArea("");
		agentLabel.setEditable(false);
		viewPanel.add(agentLabel);
		viewPanelSP = new JScrollPane(viewPanel);
		viewPanelSP.setPreferredSize(new Dimension(popGraph.getWidth(), 300));

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1,10));
		modeBtn = new JButton("Mouse Mode: Sample");
		modeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mouseMode = (++mouseMode % 3);
				switch(mouseMode)
				{
				case MOUSE_MODE_SAMPLE: modeBtn.setText("Mouse Mode: Sample"); break;
				case MOUSE_MODE_RANDOM: modeBtn.setText("Mouse Mode: Random");break;
				case MOUSE_MODE_DRAW: modeBtn.setText("Mouse Mode: Draw");break;
				default: break;
				}
			}
			
		});
		buttonPanel.add(modeBtn);
		radLabel = new JLabel("Mutation Chance: "+parentSim.mutationrate);
		
		radSlider = new JSlider();
		radSlider.setMinorTickSpacing(1);
		radSlider.setMaximum(10000);
		radSlider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent arg0) {
				parentSim.mutationrate = ((radSlider.getValue()*1.0)/(radSlider.getMaximum()*1.0)) *1.0;
				radLabel.setText("Mutation Chance: "+parentSim.mutationrate);
			}
			
		});
		radSlider.setValue((int) (radSlider.getMaximum()*parentSim.mutationrate));
		buttonPanel.add(radLabel);
		buttonPanel.add(radSlider);
		
		makeAgentBtn = new JButton("Create Agent");
		makeAgentBtn.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				final JFrame agentWindow = new JFrame("Create Agent");
				JLabel dnaLabel = new JLabel("Agent DNA:");
				JLabel genusLabel = new JLabel("Genus:");
				JLabel mutationLabel = new JLabel("Mutation rate:");
				final JTextField genusbox = new JTextField("0");
				final JTextField dnabox = new JTextField();
				final JTextField mutationbox = new JTextField();
				
				JButton addBtn = new JButton("Create Agent");
				addBtn.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						try{
							int genus = Integer.parseInt(genusbox.getText());
							double mutrate = Double.parseDouble(mutationbox.getText());
							mouseOverAgent = new AgentObject(0,0,dnabox.getText(),5000,genus,mutrate);
							mouseOverColor = new Color(genus, false);
							agentWindow.setVisible(false);
						}
						catch(Exception E)
						{
							E.printStackTrace();
						}
						
						
					}
					
				});
				agentWindow.setLayout(new GridLayout(7,1));
				Container cp = agentWindow.getContentPane();
				cp.add(dnaLabel);
				cp.add(dnabox);
				cp.add(genusLabel);
				cp.add(genusbox);
				cp.add(mutationLabel);
				cp.add(mutationbox);
				cp.add(addBtn);
				agentWindow.setSize(new Dimension(400,300));
				agentWindow.setVisible(true);
				
			}
			
		});
		buttonPanel.add(makeAgentBtn);
		
		mainPanel.add(simPanel);

		sidePanel.add(labelPanel);
		sidePanel.add(popPanel);
		sidePanel.add(viewPanelSP);

		Container content = window.getContentPane();	
		content.setLayout(new BorderLayout());
		content.add(mainPanel,BorderLayout.CENTER);
		content.add(buttonPanel, BorderLayout.SOUTH);
		content.add(sidePanel,BorderLayout.EAST);
	}


	@Override
	public void simTick(SimObject s)
	{ 
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
			return;
		
		
		BufferedImage finalFrame = new BufferedImage(image.getWidth(),image.getHeight(),BufferedImage.TYPE_INT_RGB); 
		finalFrame.setData(image.getData());
		simPanel.setImage(finalFrame);
		if(s.getTicks() % 50 == 0 && saveToFile)
		{
			saveImage((BufferedImage) image, s.getTicks()/50);
		}

		Graphics ig = image.getGraphics();
		ig.clearRect(0,0,image.getWidth(null),image.getHeight(null));

		ig.setColor(Color.black);
		ig.fillRect(0, 0, s.width+1, s.height+1);

		Graphics pg = popGraph.getGraphics();
		pg.copyArea(0, 0, popGraph.getWidth(null), popGraphHeight, -1, 0); //translate graph 1 pixel left

		Color[] pixelMap = new Color[popGraphHeight+1]; 
		pg.setColor(Color.black);
		pg.drawLine(popGraph.getWidth(null)-1,0,popGraph.getWidth(null)-1,popGraphHeight);

		int distinctSpecies = 0;
		int distinctGenomes = 0;

		for(Color c : populationMappings.keySet())		
		{

			distinctGenomes++;
			int spikeheight = (int) Math.round(((populationMappings.get(c)*1.0) / s.agents.size()*1.0) * popGraphHeight);
			if(spikeheight > popGraphHeight) //TODO: figure out what is going on here
			{
				spikeheight = 100;
			}
			if(spikeheight > 0)
			{
				distinctSpecies++;
				for(int i=spikeheight; i>=0; i--)
				{
					int pixel = popGraphHeight-i;
					if(pixel < 0)
					{
						System.out.println(i);
					}
					if(pixelMap[pixel] != c)
					{
						if((pixelMap[pixel] == null) || (populationMappings.get(pixelMap[pixel]) > populationMappings.get(c)))
						{
							pixelMap[pixel] = c;
							pg.setColor(c);
							pg.drawLine(popGraph.getWidth(null)-1, pixel, popGraph.getWidth(null)-1,  pixel);
						}
						else
						{
							break;
						}
					}
				}
			}
		}

		populationLabel.setText("Population: "+s.agents.size());
		tickLabel.setText("Ticks: "+s.getTicks());
		ticksPerSecondLabel.setText("Ticks per second: "+s.getTicksPerSecond());
		totalEnergyLabel.setText("Total energy: "+totalEnergy);
		generaLabel.setText("Genera: "+distinctGenomes);
		speciesLabel.setText("Species: "+distinctSpecies);
		highestPopLabel.setForeground(highestColor);
		highestPopLabel.setText("Highest Population: "+highestPop);

		popPanel.setImage(popGraph);

		if(mouseOverAgent != null)
		{
			agentLabel.setForeground(mouseOverColor);
			agentLabel.setText(mouseOverAgent.toString());
			/*
			ig.setColor(mouseOverColor);
			int ty = popGraph.getHeight(null)+180;
			for (String line : mouseOverAgent.toString().split("\n"))
	            ig.drawString(line, s.width+20, ty += ig.getFontMetrics().getHeight());
			 */
		}
		populationMappings.clear();

		totalEnergy = 0;
		highestPop = 0;
	}

	private Color getColor(AgentObject a)
	{
		return new Color(a.genus,false);
		//return agentColor;		
	}

	@Override
	public void agentTick(SimObject s, AgentObject a)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
			return;
		
		totalEnergy += a.energy;
		Graphics gc = image.getGraphics();
		Color agentColor = getColor(a);
		if(populationMappings.containsKey(agentColor))
		{
			int pop = populationMappings.get(agentColor);
			pop++;
			populationMappings.put(agentColor, pop);
			if(pop > highestPop)
			{
				highestPop = pop;
				highestColor = agentColor;
			}
		}
		else
		{
			populationMappings.put(agentColor,1);
		}
		gc.setColor(agentColor); 
		gc.drawLine(a.x, a.y, a.x, a.y);
		//windowgc.drawImage(tmp, 0, 0, null);

	} 

	@Override
	public void simEnd(SimObject s)
	{
		if(window != null)
		{
			System.out.println(s.ID+": Closing window...");
			window.setVisible(false);
			window = null;			
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Point pos = e.getPoint();
		if(pos.x >= 0 && pos.x <= parentSim.width && pos.y >= 0 && pos.y <= parentSim.height )
		{
			//within bounds
			Random myrandom = new Random();
			if(parentSim.positionGrid[pos.x][pos.y] == null)
			{
				AgentObject a = null;
				if(mouseMode == MOUSE_MODE_DRAW && mouseOverAgent != null)
				{
					a = new AgentObject(pos.x,pos.y,mouseOverAgent.dna,myrandom.nextInt(10000),mouseOverAgent.genus, mouseOverAgent.mutationrate);
					a.generation = mouseOverAgent.generation;
					parentSim.agents.add(a);
					parentSim.positionGrid[pos.x][pos.y] = a;
				}
				else if(mouseMode == MOUSE_MODE_RANDOM)
				{
					String randdna = "";
					int randsize = myrandom.nextInt(50)+50; //ensures every agent has at least 50 DNA, 
					for(int j=0; j<randsize; j++)
					{
						randdna += (char)(myrandom.nextInt(256));
					}
					a = new AgentObject(pos.x,pos.y,randdna,myrandom.nextInt(10000),myrandom.nextInt(), myrandom.nextDouble()*parentSim.mutationrate);
					parentSim.agents.add(a);
					parentSim.positionGrid[pos.x][pos.y] = a;
				}
				

			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		Point pos = arg0.getPoint();
		if(pos.x >= 0 && pos.x <= parentSim.width && pos.y >= 0 && pos.y <= parentSim.height )
		{
			//within bounds
			if(mouseMode == MOUSE_MODE_SAMPLE)
			{
				if(parentSim.positionGrid[pos.x][pos.y] != null)
				{
					mouseOverAgent = parentSim.positionGrid[pos.x][pos.y]; 
					mouseOverColor = new Color(parentSim.positionGrid[pos.x][pos.y].genus,false);
				}
			}
		}
	}

}

