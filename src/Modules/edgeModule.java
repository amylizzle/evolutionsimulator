package Modules;

//!!!!!!!!!!!!! CONFLICTS WITH NETWORK MODULE - DO NOT USE TOGETHER !!!!!!!!!!!!!!!!!!!!!!


import HelperFunctions.ConfigObject;
import SimComponents.AgentObject;
import SimComponents.Module;
import SimComponents.SimObject;

public class edgeModule extends Module {

	boolean enabled = false;
	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.enabled = conf.getValue("enabled").equalsIgnoreCase("true");
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Edge module disabled!");
			return;
		}
		
			
	}

	@Override
	public void agentTick(SimObject s, AgentObject a)
	{
		if(this.enabled)
		{
			if((a.x >= (s.width -1)) || (a.x <= 1) || (a.y >= (s.height -1)) || (a.y <= 1))
			{

				int oldx = a.x;
				int oldy = a.y;
				int newy = oldy;
				int newx = oldx;

				//which edge?
				if(a.x >= (s.width -1))
				{
					newx+=2;
				}
				if(a.x <= 1)
				{
					newx-= 2;
				}
				if(a.y >= (s.height -1))
				{
					newy+=2;
				}
				if(a.y <= 1)
				{
					newy-=2;
				}
				newx = (s.width+newx) % s.width;
				newy = (s.height+newy) % s.height;
				if(s.physics.doCollision(a, s.positionGrid[newx][newy], s))
				{
					s.positionGrid[oldx][oldy] = null;
					s.positionGrid[newx][newy] = a;
					a.x = newx;
					a.y = newy;
					a.energy = a.energy-10;
					//System.out.println("Moved from ("+oldx+","+oldy+") to ("+newx+","+newy+")");
				}

			}
		}
	}

}
