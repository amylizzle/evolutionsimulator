package Modules;
import HelperFunctions.ConfigObject;
import SimComponents.*; 

import java.util.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
public class guiModule extends Module implements MouseMotionListener {
	Frame window;
	Image image;
	Image popGraph;
	Hashtable<Color,Integer> populationMappings;
	int highestPop = 0;
	long totalEnergy = 0;
	Color highestColor = Color.white;
	int popGraphHeight = 100;
	private AgentObject mouseOverAgent;
	private Color mouseOverColor;
	private SimObject parentSim;
	private ConfigObject conf;



	boolean saveToFile = false; //saves the image of the sim every 50 ticks


	private void saveImage(BufferedImage bi,long frameNumber)
	{
		try {
			// retrieve image
			File outputfile = new File("image"+frameNumber+".png");
			ImageIO.write(bi, "png", outputfile); //jpegs have too many artifacts for pixel level resolution, 
			//bitmaps are too damn big.
		} catch (IOException e) {
			System.out.println("Failed to save image!");
		}
	}
	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.conf = conf;
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": GUI module disabled!");
			return;
		}
		if(conf.getValue("saveframes")!=null)
		{
			saveToFile=true;
		}
		populationMappings = new Hashtable<Color,Integer>();
		System.out.println(s.ID+": Creating Window...");
		window = new Frame("Simulation Viewer - "+s.ID);
		window.setVisible(true);
		//window.setLocation((windows.size())*s.width,10);
		window.setSize(s.width+450, s.height+200);
		window.addMouseMotionListener(this);
		image = new BufferedImage(window.getWidth(),window.getHeight(),BufferedImage.TYPE_INT_RGB);
		popGraph = new BufferedImage(400,popGraphHeight,BufferedImage.TYPE_INT_RGB);
		this.parentSim = s;	

	}


	@Override
	public void simTick(SimObject s)
	{ 
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
			return;

		Graphics gc = window.getGraphics();
		window.setBackground(Color.black);
		gc.drawImage(image, 20, 40, null);
		if(s.getTicks() % 50 == 0 && saveToFile)
		{
			saveImage((BufferedImage) image, s.getTicks()/50);
		}

		Graphics ig = image.getGraphics();
		ig.clearRect(0,0,image.getWidth(null),image.getHeight(null));

		ig.setColor(Color.black);
		ig.fillRect(0, 0, s.width+1, s.height+1);

		Graphics pg = popGraph.getGraphics();
		pg.copyArea(0, 0, popGraph.getWidth(null), popGraphHeight, -1, 0); //translate graph 1 pixel left

		Color[] pixelMap = new Color[popGraphHeight+1]; 
		pg.setColor(Color.black);
		pg.drawLine(popGraph.getWidth(null)-1,0,popGraph.getWidth(null)-1,popGraphHeight);

		int distinctSpecies = 0;
		int distinctGenomes = 0;

		for(Color c : populationMappings.keySet())		
		{

			distinctGenomes++;
			int spikeheight = (int) Math.round(((populationMappings.get(c)*1.0) / s.agents.size()*1.0) * popGraphHeight);
			if(spikeheight > popGraphHeight) //TODO: figure out what is going on here
			{
				spikeheight = 100;
			}
			if(spikeheight > 0)
			{
				distinctSpecies++;
				for(int i=spikeheight; i>=0; i--)
				{
					int pixel = popGraphHeight-i;
					if(pixel < 0)
					{
						System.out.println(i);
					}
					if(pixelMap[pixel] != c)
					{
						if((pixelMap[pixel] == null) || (populationMappings.get(pixelMap[pixel]) > populationMappings.get(c)))
						{
							pixelMap[pixel] = c;
							pg.setColor(c);
							pg.drawLine(popGraph.getWidth(null)-1, pixel, popGraph.getWidth(null)-1,  pixel);
						}
						else
						{
							break;
						}
					}
				}
			}
		}
		ig.setColor(Color.white);

		ig.drawString("Population: "+s.agents.size(), s.width+20, 20);
		ig.drawString("Ticks: "+s.getTicks(), s.width+20, 40);

		ig.drawString("Ticks per second: "+s.getTicksPerSecond(), s.width+20, 60);
		ig.drawString("Total Energy: "+totalEnergy, s.width+20, 80);
		ig.drawString("Distinct genera: "+distinctGenomes,  s.width+20, 100);
		ig.drawString("Distinct species: "+distinctSpecies,  s.width+20, 120);
		ig.setColor(highestColor);
		ig.drawString("Highest population: "+highestPop, s.width+20, 140);
		ig.drawImage(popGraph, s.width+20, 160, null);
		if(mouseOverAgent != null)
		{
			ig.setColor(mouseOverColor);
			int ty = popGraph.getHeight(null)+180;
			for (String line : mouseOverAgent.toString().split("\n"))
				ig.drawString(line, s.width+20, ty += ig.getFontMetrics().getHeight());
		}
		populationMappings.clear();

		totalEnergy = 0;
		highestPop = 0;

	}

	private Color getColor(AgentObject a)
	{
		return new Color(a.genus,false);
		//return agentColor;		
	}

	@Override
	public void agentTick(SimObject s, AgentObject a)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
			return;

		totalEnergy += a.energy;
		Graphics gc = image.getGraphics();
		Color agentColor = getColor(a);
		if(populationMappings.containsKey(agentColor))
		{
			int pop = populationMappings.get(agentColor);
			pop++;
			populationMappings.put(agentColor, pop);
			if(pop > highestPop)
			{
				highestPop = pop;
				highestColor = agentColor;
			}
		}
		else
		{
			populationMappings.put(agentColor,1);
		}
		gc.setColor(agentColor); 
		gc.drawLine(a.x, a.y, a.x, a.y);
		//windowgc.drawImage(tmp, 0, 0, null);

	} 

	@Override
	public void simEnd(SimObject s)
	{
		if(window != null)
		{
			System.out.println(s.ID+": Closing window...");
			window.setVisible(false);
			window = null;			
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Point pos = e.getPoint();
		pos.x -= 20;
		pos.y -= 40; //compensate for image draw shift
		//mouseOverAgent = pos.toString();
		if(pos.x >= 0 && pos.x <= parentSim.width && pos.y >= 0 && pos.y <= parentSim.height )
		{
			//within bounds
			Random myrandom = new Random();
			if(parentSim.positionGrid[pos.x][pos.y] == null)
			{
				AgentObject a;
				if(((e.getModifiers() & 4) != 0) && (mouseOverAgent != null))
				{
					a = new AgentObject(pos.x,pos.y,mouseOverAgent.dna,myrandom.nextInt(10000),mouseOverAgent.genus, mouseOverAgent.mutationrate);
					a.generation = mouseOverAgent.generation;

				}
				else
				{
					String randdna = "";
					int randsize = myrandom.nextInt(50)+50; //ensures every agent has at least 50 DNA, 
					for(int j=0; j<randsize; j++)
					{
						randdna += (char)(myrandom.nextInt(256));
					}
					a = new AgentObject(pos.x,pos.y,randdna,myrandom.nextInt(10000),myrandom.nextInt(), myrandom.nextDouble()*parentSim.mutationrate);
				}
				parentSim.agents.add(a);
				parentSim.positionGrid[pos.x][pos.y] = a;

			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		Point pos = arg0.getPoint();
		pos.x -= 20;
		pos.y -= 40; //compensate for image draw shift
		//mouseOverAgent = pos.toString();
		if(pos.x >= 0 && pos.x <= parentSim.width && pos.y >= 0 && pos.y <= parentSim.height )
		{
			//within bounds

			if(parentSim.positionGrid[pos.x][pos.y] != null)
			{
				mouseOverAgent = parentSim.positionGrid[pos.x][pos.y]; 
				mouseOverColor = new Color(parentSim.positionGrid[pos.x][pos.y].genus,false);
			}
		}
		//System.out.println(pos);
	}

}

