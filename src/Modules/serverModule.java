package Modules;
import HelperFunctions.ConfigObject;
import SimComponents.*;

import java.util.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
public class serverModule extends Module {
	Image image;
	Image popGraph;
	Hashtable<Color,Integer> populationMappings;
	int highestPop = 0;
	long totalEnergy = 0;
	Color highestColor = Color.white;
	int popGraphHeight = 100;
	ConfigObject conf;




	boolean saveToFile = true; //saves the image of the sim every 50 ticks
	//so I could use ffmpeg to make it into a movie for Little Chip
	//FUTURE: make this a command line switch/config file?

	private void saveImage(BufferedImage bi)
	{
		try {
			// retrieve image
			String iPath = "image.png";
			if(conf.getValue("imagepath")!=null)
			{
				iPath = conf.getValue("imagepath");
			}
			File outputfile = new File(iPath);
			ImageIO.write(bi, "png", outputfile); //jpegs have too many artifacts for pixel level resolution, 
			//bitmaps are too damn big.
		} catch (IOException e) {
			System.out.println("Failed to save image!");
		}
	}
	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.conf = conf;
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Server module disabled!");
			return;
		}
		populationMappings = new Hashtable<Color,Integer>();
		image = new BufferedImage(s.width+450,s.height+2,BufferedImage.TYPE_INT_RGB);
		popGraph = new BufferedImage(400,popGraphHeight,BufferedImage.TYPE_INT_RGB);
	}


	@Override
	public void simTick(SimObject s)
	{ 
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			return;
		}
		
		if(s.getTicks() % 10 == 0 && saveToFile)
		{
			saveImage((BufferedImage) image);
		}



		Graphics ig = image.getGraphics();
		ig.clearRect(0,0,image.getWidth(null),image.getHeight(null));

		ig.setColor(Color.black);
		ig.fillRect(0, 0, s.width+1, s.height+1);

		Graphics pg = popGraph.getGraphics();
		pg.copyArea(0, 0, popGraph.getWidth(null), popGraphHeight, -1, 0); //translate graph 1 pixel left

		Color[] pixelMap = new Color[popGraphHeight+1]; 
		pg.setColor(Color.black);
		pg.drawLine(popGraph.getWidth(null)-1,0,popGraph.getWidth(null)-1,popGraphHeight);

		int distinctSpecies = 0;
		int distinctGenomes = 0;

		for(Color c : populationMappings.keySet())		
		{

			distinctGenomes++;
			int spikeheight = (int) Math.round(((populationMappings.get(c)*1.0) / s.agents.size()*1.0) * popGraphHeight);
			if(spikeheight > popGraphHeight) //TODO: figure out what is going on here
			{
				spikeheight = 100;
			}
			if(spikeheight > 0)
			{
				distinctSpecies++;
				for(int i=spikeheight; i>=0; i--)
				{
					int pixel = popGraphHeight-i;
					if(pixel < 0)
					{
						System.out.println(i);
					}
					if(pixelMap[pixel] != c)
					{
						if((pixelMap[pixel] == null) || (populationMappings.get(pixelMap[pixel]) > populationMappings.get(c)))
						{
							pixelMap[pixel] = c;
							pg.setColor(c);
							pg.drawLine(popGraph.getWidth(null)-1, pixel, popGraph.getWidth(null)-1,  pixel);
						}
						else
						{
							break;
						}
					}
				}
			}
		}
		ig.setColor(Color.white);
		ig.setFont(new Font("Dialog",Font.PLAIN,12));
		ig.drawString("Population: "+s.agents.size(), s.width+20, 20);
		ig.drawString("Ticks: "+s.getTicks(), s.width+20, 40);
		ig.drawString("Ticks per second: "+s.getTicksPerSecond(), s.width+20, 60);
		ig.drawString("Total Energy: "+totalEnergy, s.width+20, 80);
		ig.drawString("Distinct genera: "+distinctGenomes,  s.width+20, 100);
		ig.drawString("Distinct species: "+distinctSpecies,  s.width+20, 120);
		ig.setColor(highestColor);
		ig.drawString("Highest population: "+highestPop, s.width+20, 140);
		ig.drawImage(popGraph, s.width+20, 160, null);
		populationMappings.clear();

		totalEnergy = 0;
		highestPop = 0;

	}

	private Color getColor(AgentObject a)
	{
		return new Color(a.genus,false);
		//return agentColor;		
	}

	@Override
	public void agentTick(SimObject s, AgentObject a)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			return;
		}

		totalEnergy += a.energy;
		Graphics gc = image.getGraphics();
		Color agentColor = getColor(a);
		if(populationMappings.containsKey(agentColor))
		{
			int pop = populationMappings.get(agentColor);
			pop++;
			populationMappings.put(agentColor, pop);
			if(pop > highestPop)
			{
				highestPop = pop;
				highestColor = agentColor;
			}
		}
		else
		{
			populationMappings.put(agentColor,1);
		}
		gc.setColor(agentColor); 
		gc.drawLine(a.x, a.y, a.x, a.y);
		//windowgc.drawImage(tmp, 0, 0, null);


	} 

}

