package Modules; 
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import HelperFunctions.ConfigObject;
import SimComponents.AgentObject;
import SimComponents.Module;
import SimComponents.SimObject;

public class recordingModule extends Module {
	FileOutputStream recordbuffer;
	ZipOutputStream zipbuffer;
	ConfigObject conf; 

	final int recordingPeriod = 100;

	@Override 
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.conf = conf;
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Recording module disabled!");
			return;
		}

		String filename = ((int) (System.currentTimeMillis() / 1000L))+"-"+s.mutationrate+"-"+s.width+"x"+s.height+"r"+s.ID+".sim";
		try {
			recordbuffer = new FileOutputStream(filename);
			zipbuffer = new ZipOutputStream(recordbuffer);


			ByteArrayOutputStream bytebuffer = new ByteArrayOutputStream();
			ObjectOutputStream recordObjectBuffer = new ObjectOutputStream(bytebuffer);
			//format is: [tick count, system time, simulation state]
			recordObjectBuffer.writeLong(s.getTicks());
			recordObjectBuffer.writeLong(System.currentTimeMillis());
			synchronized(s)
			{
				recordObjectBuffer.writeObject(s);
			}
			recordObjectBuffer.flush();
			ZipEntry ze = new ZipEntry(Long.toString(s.getTicks()));
			ze.setSize(bytebuffer.size());
			zipbuffer.putNextEntry(ze);
			zipbuffer.write(bytebuffer.toByteArray());

			recordObjectBuffer.close();			
			zipbuffer.closeEntry();

			System.out.println(s.ID+": Recording to file:"+filename+" every "+recordingPeriod+" frames");
		} catch (Exception e) {
			System.out.println(s.ID+": Failed to create record file: "+filename);
			e.printStackTrace();			
		}
	}

	@Override
	public void simTick(SimObject s)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
			return;

		if(s.getTicks() % recordingPeriod == 0)
		{
			try {
				ByteArrayOutputStream bytebuffer = new ByteArrayOutputStream();
				ObjectOutputStream recordObjectBuffer = new ObjectOutputStream(bytebuffer);
				recordObjectBuffer.writeLong(s.getTicks());
				recordObjectBuffer.writeLong(System.currentTimeMillis());
				synchronized(s)
				{
					recordObjectBuffer.writeObject(s);
				}
				recordObjectBuffer.flush();
				ZipEntry ze = new ZipEntry(Long.toString(s.getTicks()/recordingPeriod));
				ze.setSize(bytebuffer.size());
				zipbuffer.putNextEntry(ze);
				zipbuffer.write(bytebuffer.toByteArray());

				recordObjectBuffer.close();			
				zipbuffer.closeEntry();
			} catch (Exception e) {				
				e.printStackTrace();
			}
		}

	}

	@Override
	public void agentTick(SimObject s, AgentObject a)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("true"))
		{
			if((s.getTicks() % recordingPeriod == 1) && (s.getTicks() > recordingPeriod))
			{
				//iterate through ancestry tree, remove anything older than recordingPeriod
				a.getAncestry().prune(); //remove all from the tree except self			
			}
		}
		else
		{
			a.getAncestry().prune(); //delete ancestry all the time
		}
	}



	@Override
	public void simEnd(SimObject s)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("true"))
		{
			try {
				//zipbuffer.closeEntry();
				zipbuffer.flush();
				zipbuffer.close(); //in java, close() calls to all connected streams, so this will close recordfile too
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
