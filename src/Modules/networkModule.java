package Modules;

import java.util.Hashtable;

import HelperFunctions.ConfigObject;
import NetworkComponents.NetworkModuleThread;
import SimComponents.*;

public class networkModule extends Module {


	private NetworkModuleThread net;
	private Hashtable<Integer,Integer> popTable; //Genus, count;
	private long lastStatTime = 0;
	private ConfigObject conf;

	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.conf = conf;
		//create network thread
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Network module disabled!");
			return;
		}

		net = new NetworkModuleThread();
		net.initialise(s.width, s.height, s.ID,conf);
		net.start();
		popTable = new Hashtable<Integer,Integer>();
		lastStatTime = System.currentTimeMillis();


	}

	@Override
	public void simTick(SimObject s) 
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
			return;

		if(!net.isDead())
		{
			if((System.currentTimeMillis() - lastStatTime) > 300000) //5min*60sec*1000milli
			{
				net.sendStats(popTable,s.ID);
				lastStatTime = System.currentTimeMillis();
			}
			popTable = new Hashtable<Integer,Integer>(); //don't use clear, create a new object so as not to affect the passed popTable
			//garbage collector will clean up when it's done.
			AgentObject[] incomers = net.getIncomingAgents();
			for(int i=0; i<incomers.length; i++)
			{
				//validate agent
				if((incomers[i].x < s.width) && (incomers[i].x > 0) && (incomers[i].y < s.height) && (incomers[i].y > 0))
				{
					if(s.physics.doCollision(incomers[i], s.positionGrid[incomers[i].x][incomers[i].y], s))
					{
						s.positionGrid[incomers[i].x][incomers[i].y]=incomers[i];
						s.agents.add(incomers[i]);
						//System.out.println("Agent came in:"+incomers[i]);
					}
					else
					{
						//send it back
						net.sendAgent(incomers[i]);
					}
				}
			}
		}
		else
		{ //unrecoverable error occurred, plz try again
			net.killThread();
			System.out.println(s.ID+": Network thread died! Trying again!");
			net = new NetworkModuleThread();
			net.initialise(s.width, s.height,s.ID,conf);
			net.start();
		}

	}

	@Override
	public void agentTick(SimObject s, AgentObject a)
	{
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
			return;

		Integer count = popTable.get(a.genus);
		if(count == null)
		{
			popTable.put(a.genus,1);
		}
		else
		{
			popTable.put(a.genus, count+1);
		}
		if(net.isReady())
		{
			//is agent at the edge?
			if((a.x >= (s.width -1)) || (a.x <= 1) || (a.y >= (s.height -1)) || (a.y <= 1))
			{
				if(!net.isDead())
				{
					//send it upstream and kill it here				
					AgentObject b = new AgentObject(a); 
					net.sendAgent(b);
					s.killAgent(a);
				}
				else
				{   //unrecoverable error occurred, plz try again
					System.out.println(s.ID+": Network thread died! Trying again!");
					net.killThread();
					net = new NetworkModuleThread();
					net.initialise(s.width, s.height,s.ID,conf);
					net.start();
				}
			}
		}

	}

	@Override
	public void simEnd(SimObject s)
	{
		if(net != null)
			net.killThread();			
	}

}
