package Modules; 
import HelperFunctions.ConfigObject;
import SimComponents.AgentObject;
import SimComponents.Module;
import SimComponents.SimObject;
public class actionsModule extends Module {
	
	boolean enabled = false;
	
	private char getSeenState(AgentObject a, SimObject s)
	{
		int modx = 0;
		int mody = 0;
		switch(a.getRotation())
		{
		case 'N': mody = -1; break;
		case 'E': modx = 1; break;
		case 'S': mody = 1; break;
		case 'W': modx = -1; break;
		}
		for(int i=1; i<a.getSeeDepth();i++)
		{
			int tmpx = (s.width+a.x+(i*modx))%s.width;
			int tmpy = (s.height+a.y+(i*mody))%s.height;
			if(s.positionGrid[tmpx][tmpy] != null)
			{
				return s.positionGrid[tmpx][tmpy].getState();
			}
		}
		return '\0';
	}
	
	private int getSeenMarkerID(AgentObject a, SimObject s)
	{
		int modx = 0;
		int mody = 0;
		switch(a.getRotation())
		{
		case 'N': mody = -1; break;
		case 'E': modx = 1; break;
		case 'S': mody = 1; break;
		case 'W': modx = -1; break;
		}
		for(int i=1; i<a.getSeeDepth();i++)
		{
			int tmpx = (s.width+a.x+(i*modx))%s.width;
			int tmpy = (s.height+a.y+(i*mody))%s.height;
			if(s.positionGrid[tmpx][tmpy] != null)
			{
				return s.positionGrid[tmpx][tmpy].idMarker;
			}
		}
		return 0;
	}
	
	private int getSeenEnergy(AgentObject a, SimObject s)
	{
		int modx = 0;
		int mody = 0;
		switch(a.getRotation())
		{
		case 'N': mody = -1; break;
		case 'E': modx = 1; break;
		case 'S': mody = 1; break;
		case 'W': modx = -1; break;
		}
		for(int i=1; i<a.getSeeDepth();i++)
		{
			int tmpx = (s.width+a.x+(i*modx))%s.width;
			int tmpy = (s.height+a.y+(i*mody))%s.height;
			if(s.positionGrid[tmpx][tmpy] != null)
			{
				return s.positionGrid[tmpx][tmpy].energy;
			}
		}
		return 0;
	}
	
	private int getEyesight(AgentObject a, SimObject s)
	{
		int modx = 0;
		int mody = 0;
		switch(a.getRotation())
		{
		case 'N': mody = -1; break;
		case 'E': modx = 1; break;
		case 'S': mody = 1; break;
		case 'W': modx = -1; break;
		}
		for(int i=1; i<a.getSeeDepth();i++)
		{
			int tmpx = (s.width+a.x+(i*modx))%s.width;
			int tmpy = (s.height+a.y+(i*mody))%s.height;
			if(s.positionGrid[tmpx][tmpy] != null)
			{
				return i;
			}
		}
		return 0;
	}
	
	private void doDNATick(AgentObject a, SimObject s)
	{
		int commandsprocessed = 0;
		int maxcommandspertick = 50; //50
		
		if(a.getChloroplasts() > 0)
		{
			//You are a veg, so you get less DNA to play with
			maxcommandspertick = 2;
		}
		while((a.dna.length()>0) && (commandsprocessed<maxcommandspertick)) //has DNA to process
		{
			
			int tmpdnapos = 0;
			
			if(a.dnapos>=a.dna.length())
			{
				a.dnapos = 0;
			}
			
			
			char dnapiece = a.dna.charAt(a.dnapos);
			commandsprocessed++;
			switch(dnapiece) 
			{
				case 'a': s.physics.doMovement('N',a,s); break; //move north
				case 'b': s.physics.doMovement('S',a,s); break; //move south
				case 'c': s.physics.doMovement('W',a,s); break; //move west
				case 'd': s.physics.doMovement('E',a,s); break; //move east
				case 'j': s.physics.doMovement((char)a.stackPop(),a,s); break; //set movement to stackPop				
				case 'e': a.setRotation('N'); break;
				case 'f': a.setRotation('E'); break;
				case 'g': a.setRotation('S'); break;
				case 'h': a.setRotation('W'); break;
				case 'i': a.setRotation((char)a.stackPop()); break; //set rotation to stackPop
				case 'k': s.physics.breedAgents(a,null,s); break;  //split asexually 
				case 'l': a.setState('M'); break;  //mate 
				case 'm': a.setState('S'); break; //shell
				case 'n': a.setState('E'); break; //eat
				case 'o': a.setState('V'); break; //become a vegetable
				case 'p': a.setState((char)a.stackPop()); break; //pop from stack into state
				case 'q': a.idMarker = a.stackPop(); break;//pop from stack into idmarker
				case 'r': a.setSight(a.stackPop()); break;//set sight distance
				case 's': a.stackPush(a.dnapos); break;//get dnapos		
				case 't': a.stackPush(getEyesight(a,s)); break; //attempt to look for agents in the direction I'm facing
				case 'u': a.stackPush(a.getRotation()); break; //push my rotation onto stack
				case 'v': a.stackPush(a.getState()); break; //push my state onto stack
				case 'w': a.stackPush(getSeenState(a,s)); break; //push state of agent I'm looking at onto stack
				case 'x': a.stackPush(getSeenMarkerID(a,s)); break; //push marker ID of agent I'm looking at onto stack
				case 'y': a.stackPush(a.getAge()); break; //push age onto stack
				case 'z': a.stackPush(a.energy); break; //push energy onto stack
				case 'A': if (a.stackPop() < a.stackPop()) { a.jump(a.stackPop()-1); } break;//JLT
				case 'B': if (a.stackPop() > a.stackPop()) { a.jump(a.stackPop()-1); } break; //JGT
				case 'C': a.jump(a.stackPop()-1); break; //JMP
				case 'D': if(a.stackPop() == 0) { a.jump(a.stackPop()-1); } break; //JZ
				case 'E': if(a.stackPop() != 0) { a.jump(a.stackPop()-1); } break; //JNZ
				case 'F': a.jump(a.dnapos + a.stackPop()); break; //short jump
				case 'G': //push next DNA byte & skip
					tmpdnapos = a.dnapos+1;
					tmpdnapos = tmpdnapos % a.dna.length();
					a.stackPush(a.dna.charAt(tmpdnapos)); // System.out.println("Pushed "+a.dna.charAt(tmpdnapos));
					a.dnapos = tmpdnapos; //gets incremented at end of this case
					break;
				case 'H': a.stackPush(a.stackPop() + a.stackPop()); break; //add
				case 'I': a.stackPush(a.stackPop() - a.stackPop()); break; //subtract
				case 'J': a.stackPush(a.stackPop() * a.stackPop()); break; //multiply
				case 'K': //divide
					int numOne = a.stackPop();
					int numTwo = a.stackPop();
					if(numTwo != 0)
						a.stackPush(numOne / numTwo);
					else
						a.stackPush(Integer.MAX_VALUE); //divide by 0 = inf
					break;
				case 'L':a.stackPop(); break;//clear top of stack
				case 'M':a.stackPush(a.idMarker); break;//push my ID marker onto stack
				case 'N':a.stackPush(getSeenEnergy(a,s)); break;//push energy of agent I'm looking at onto stack
				case 'O':a.stackPush(a.chloroplasts); break;//count my chloro's
				case 'P':a.stackPush(a.shell); break;//count my shell
				default : break;
			}
			a.dnapos++;		
			
		}
		
	}
	
	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.enabled = conf.getValue("enabled").equalsIgnoreCase("true");
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Actions module disabled!");
			return;
		}
			
	}
	
	@Override
	public void agentTick(SimObject s, AgentObject a)
	{
		if(enabled)
		{
			doDNATick(a,s);		
		}
	}
	

}
