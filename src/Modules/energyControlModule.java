package Modules;

import HelperFunctions.ConfigObject;
import SimComponents.AgentObject;
import SimComponents.Module;
import SimComponents.SimObject;

public class energyControlModule extends Module {
	int sunCycleDuration = 500;
	int vegEnergyBoost = 1;
	int agingCost = 1;
	int chlorocost = 10;
	int shellcost = 10;
	int maxshell = 1000;
	int maxchloro = 10;
	int chlororate = 10;
	int shellrate = 10;
	int chlorodegrate = 10;
	int shelldegrate = 10;
	boolean enabled = false;

	@Override
	public void simInit(SimObject s,ConfigObject conf)
	{
		this.enabled = conf.getValue("enabled").equalsIgnoreCase("true");
		if(conf.getValue("enabled").equalsIgnoreCase("false"))
		{
			System.out.println(s.ID+": Energy Control module disabled!");
			return;
		}
		if(conf.getValue("suncycleduration") != null)
		{
			sunCycleDuration = Integer.parseInt(conf.getValue("suncycleduration"));
		}
		if(conf.getValue("vegenergyboost") != null)
		{
			vegEnergyBoost = Integer.parseInt(conf.getValue("energyperchloro"));
		}
		if(conf.getValue("agingcost") != null)
		{
			agingCost = Integer.parseInt(conf.getValue("agingcost"));
		}
		if(conf.getValue("chlorocost") != null)
		{
			chlorocost = Integer.parseInt(conf.getValue("chlorocost"));
		}
		if(conf.getValue("shellcost") != null)
		{
			shellcost = Integer.parseInt(conf.getValue("shellcost"));
		}
		if(conf.getValue("chlororate") != null)
		{
			chlororate = Integer.parseInt(conf.getValue("chlororate"));
		}
		if(conf.getValue("shellrate") != null)
		{
			shellrate = Integer.parseInt(conf.getValue("shellrate"));
		}
		if(conf.getValue("maxshell") != null)
		{
			maxshell = Integer.parseInt(conf.getValue("maxshell"));
		}
		if(conf.getValue("maxchloro") != null)
		{
			maxchloro = Integer.parseInt(conf.getValue("maxchloro"));
		}
		if(conf.getValue("shelldegrate") != null)
		{
			shelldegrate = Integer.parseInt(conf.getValue("shelldegrate"));
		}
		if(conf.getValue("chlorodegrate") != null)
		{
			chlorodegrate = Integer.parseInt(conf.getValue("chlorodegrate"));
		}
	}

	@Override	
	public void agentTick(SimObject s, AgentObject a)
	{
		if(enabled)
		{
			a.incAge();

			if(a.getChloroplasts() > 0)
			{
				if((s.getTicks() / sunCycleDuration) % 2 == 1) //the sun is up
					a.energy+=(vegEnergyBoost * a.getChloroplasts());
			}
			//this isn't an else because all agents lose energy from aging, veg just get a boost.
			a.energy -= agingCost;


			if(a.getState() == 'V')
			{
				if((a.getChloroplasts() < maxchloro) && (a.getAge() % chlororate == 0))
				{
					a.chloroplasts++;
					a.energy -= chlorocost;
				}
			}


			if(a.chloroplasts > 0)
			{
				if(a.getAge() % chlorodegrate == 0)
					a.chloroplasts--;
			}


			if(a.getState() == 'S')
			{
				if((a.getShell()<maxshell) && (a.getAge() % shellrate == 0))
				{
					a.shell++;
					a.energy -= shellcost;
				}
			}
			
			if(a.getShell() > 0)
			{
				if(a.getAge() % shelldegrate == 0)
					a.shell--;
			}

		}

	}
}
