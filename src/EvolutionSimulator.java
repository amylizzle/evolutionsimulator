import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;

import HelperFunctions.ConfigObject;
import HelperFunctions.ModuleLoader;
import SimComponents.AgentObject;
import SimComponents.Module;
import SimComponents.SimObject;

public class EvolutionSimulator {
	final static String helpstring = "Commands available:" +
			"\nexit: exits the program" +
			"\nhelp: displays this help"+
			"\nsimstat: displays stats about sims runnning"+
			"\ngetoldest: displays oldest agents in running sims"+
			"\ngetbest: displays agent that has eaten the most energy in its life";

	public static void main(String[] args) {
		int numthreads = 0; 
		int simheight = 0;
		int simwidth = 0;
		double simmutation = 0;
		try//check args
		{
			numthreads = Integer.parseInt(args[0]);
			simwidth = Integer.parseInt(args[1]);
			simheight = Integer.parseInt(args[2]);
			simmutation = Double.parseDouble(args[3]);
			//System.out.println("Mutation chance: "+simmutation);
		}
		catch(Exception E)
		{
			System.out.println(E.toString());
			System.out.println("Usage: EvolutionSimulator <number_of_simulations> <width_of_sim> <height_of_sim> <mutation_rate>");
			System.exit(1);
		}
		
	
		SimObject[] sims = new SimObject[numthreads];
		String moduleDirectory = System.getProperty("user.dir")+"/bin/Modules/";  //C:\\simmods\\
		System.out.println("Loading modules from "+moduleDirectory);
		Class<Module>[] simModules = ModuleLoader.loadModules(moduleDirectory);
		System.out.println("Found "+simModules.length+" modules");
		Hashtable<String,ConfigObject> configs = ModuleLoader.loadConfigs("config.ini");
		if(configs == null)
		{
			return;
		}
		System.out.println("Starting sims...");
		for(int i=0; i<numthreads; i++)
		{
			sims[i] = new SimObject(simheight,simwidth,simmutation,simModules,configs);
			sims[i].initialize();
			sims[i].start();
		}
		
		//all sims created and initialized;
		//execute till exit
//		System.out.println("Type exit to end. Type help to see console commands.");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		boolean stillalive = true;
		while(stillalive)
		{
			stillalive = false;
			for(int i=0; i<sims.length; i++)
			{
				if(sims[i].isAlive())
				{
					stillalive = true;
				}
				else
				{
					sims[i] = new SimObject(simheight,simwidth,simmutation,simModules,configs);
					sims[i].initialize();
					sims[i].start();
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				
			}
			String command = "";
			try {
				if(br.ready())
					command = br.readLine();
			} catch (IOException e) {
				//nothing
			}
			if (command.length() > 0)
			{
				if(command.equals("exit"))
				{
					break;
				}
				else if (command.equals("help"))
				{
					System.out.println(helpstring);
				}	 
				else if (command.equals("simstat"))
				{
					for(int j=0; j<sims.length; j++)
					{
						if(sims[j].isAlive())
						{
							long seconds = sims[j].getRunTime();
							long minutes = seconds / 60;
							seconds = seconds % 60;
							long hours = minutes / 60;
							minutes = minutes % 60;
							
							System.out.println(sims[j].ID+": Runtime: "+hours+"h:"+minutes+"m:"+seconds+"s");
							System.out.println(sims[j].ID+": Population: "+sims[j].agents.size());
							System.out.println(sims[j].ID+": Ticks: "+sims[j].getTicks());
							System.out.println(sims[j].ID+": Ticks per second: "+sims[j].getTicksPerSecond());
							System.out.println(sims[j].ID+": Average ticks per second: "+sims[j].getAvgTicksPerSecond());
						}
					}
				}
				else if (command.equals("getoldest")) //getoldest and getbest ARE NOT THREAD SAFE - wrapped in try's for now
				{
					try{
						for(int j=0; j<sims.length; j++)
						{
							if(sims[j].isAlive())
							{
								AgentObject a = new AgentObject();
								for(int k=0; k<sims[j].agents.size(); k++)
								{
									if(a.getAge()<sims[j].agents.get(k).getAge())
									{
										a = sims[j].agents.get(k); 
									}
								}
								System.out.println(sims[j].ID+": Oldest Agent: "+a);	
							}
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					
					
				}
				else if (command.equals("getbest"))
				{
					try{
						for(int j=0; j<sims.length; j++)
						{
							if(sims[j].isAlive())
							{
								AgentObject a = new AgentObject();
								double ascore = 0;
								for(int k=0; k<sims[j].agents.size(); k++)
								{
									AgentObject b = sims[j].agents.get(k);
									double bscore = (b.getAte()*1.0)/((b.getAge()*1.0)/((b.getBred()+1)*1.0));
									
									if(ascore < bscore)
									{
										a = b; 
										ascore = bscore;
									}
								}
								System.out.println(sims[j].ID+": Best Agent: "+a+"\nScore:"+ascore);	
							}
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					System.out.println("Unkown command: "+command);
				}
			}
		}
		
		for(int i=0; i<numthreads; i++)
		{
			sims[i].end();
		}
		while(stillalive)
		{
			stillalive = false;
			for(int i=0; i<sims.length; i++)
			{
				if(sims[i].isAlive())
				{
					stillalive = true;
				}
			}
		}
		System.out.println("Application ended");
		System.exit(0);
	}

}
