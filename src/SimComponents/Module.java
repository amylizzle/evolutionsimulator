package SimComponents;

import HelperFunctions.ConfigObject;

public abstract class Module {
	public void simInit(SimObject s,ConfigObject conf)
	{
		
	}
	
	public void simTick(SimObject s)
	{
		
	}
	
	public void agentTick(SimObject s, AgentObject a)
	{
		
	}
	
	public void simEnd(SimObject s)
	{
		
	}
	
	
}
