package SimComponents;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.io.Serializable;

import HelperFunctions.ConfigObject;

public class SimObject extends Thread implements Serializable {

	private static final long serialVersionUID = 9031280818674869096L;
	//dynamic array to store agents (O(1) access & O(n) insert in worst case, ie: needs to expand array beyond reserved mem);
	public volatile ArrayList<AgentObject> agents = new ArrayList<AgentObject>();
	private ArrayList<AgentObject> toRemove = new ArrayList<AgentObject>();
	public volatile transient AgentObject[][] positionGrid;
	public double mutationrate;
	public int height;
	public int width;
	public int ID;
	private long ticks;
	private Class<Module>[] moduleClasses;
	public transient Module[] modules; //transient because no need to serialise the module instances. Classes are stored above anyways
	public Hashtable<String,ConfigObject> configs;
	private transient float ticksPerSecond = 0;
	private transient long lastTickTime = 0;
	private transient long absoluteStartTime = 0;
	
	public transient PhysicsHandler physics;
	
	public SimObject(int height, int width, double mutationRate, Class<Module>[] modules, Hashtable<String,ConfigObject> configs)
	{
		this.ticks = 0;
		this.width = width;
		this.height = height;
		this.mutationrate = mutationRate;
		positionGrid = new AgentObject[width+1][height+1];
		this.moduleClasses = modules;
		this.modules = new Module[modules.length];
		this.configs = configs;
		System.out.println("Simulation created...");
	}
	
	public void initialize()
	{
		Random mr = new Random();
		ID = mr.nextInt(Integer.MAX_VALUE); //between 0 & maxval
		
		System.out.println(ID+": Simulation initialzing...");
		
		//load modules
		for(int i=0; i<moduleClasses.length; i++)
		{
			try {							
				modules[i] = moduleClasses[i].newInstance();
				System.out.println(ID+": Loading module: "+modules[i].getClass().getName());
				ConfigObject co = configs.get(modules[i].getClass().getName());
				if(co == null)
					co = new ConfigObject(modules[i].getClass().getName());
				modules[i].simInit(this,co);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//check to see if a physics handler was loaded, and if not, make a place holder
		if(physics == null)
		{
			physics = new PhysicsHandler();
		}
		
		for(int i=0; i<agents.size(); i++)
		{
			positionGrid[agents.get(i).x][agents.get(i).y] = agents.get(i);
		}
	}

	public void end() 
	{
		System.out.println(ID+": Simulation shutting down...");
		this.interrupt();
	}
	
	public void run()
	{
		int i=0;
		int j=0;
		int k=0;
		int exceptionsInARow = 0;
		System.out.println(ID+": Simulation started!");
		absoluteStartTime = System.nanoTime();
		while(!this.isInterrupted())
		{
			try
			{

				//	if(agents.size() == 0) 
				//	{
				//		System.out.println("Errybody dead");
				//		this.interrupt();
				//	}
				long nanospertick = (System.nanoTime()-lastTickTime);
				if(nanospertick == 0) {nanospertick = 1; } //just in case, otherwise div by 0
				ticksPerSecond = 1.0f/(nanospertick/1000000000.0f); //1 divided by seconds per tick = ticks per second
				lastTickTime = System.nanoTime();
				//do methods for each sim tick
				ticks++;
				for(k=0; k<modules.length; k++)
				{
					modules[k].simTick(this);
				}

				for(i=0; i<agents.size(); i++)
				{
					AgentObject a = agents.get(i);

					//do agent tick methods
					for(j=0; j<modules.length; j++)
					{
						if(a.energy>=0) //modules could kill agent
						{
							modules[j].agentTick(this, a);
							if(a.energy <= 0)
							{
								//if module resulted in agent running out of energy, kill it.
								killAgent(a);
							}

						}
						else
							break; //agent is dead, no point continuing to do stuff to it
					}
				}

				//agents processed, remove the dead
				for(int p=0; p<toRemove.size(); p++)
				{
					agents.remove(toRemove.get(p));
				}
				toRemove.clear();
			}	
			catch(Exception e)
			{
				exceptionsInARow++;
				if(k<modules.length) { System.out.println(ID+": simTick() in "+modules[k].getClass().getName()+" caused failure"); }
				if(j<modules.length) { System.out.println(ID+": agentTick() in "+modules[j].getClass().getName()+" caused failure"); }

				if(exceptionsInARow > 3)
				{
					System.out.println(ID+": Simulation died after "+exceptionsInARow+" failures with error:"+e);
					System.out.println(ID+": Trying to unload modules cleanly.");
					//finalise modules when sim ends
					this.interrupt();
					for(i=0; i<modules.length; i++)
					{
						try{
							modules[i].simEnd(this);
						}
						catch(Exception me){
							System.out.println(ID+": Module "+modules[i].getClass().getName()+" failed to unload with error: "+me);
						}
					}
				}				
				e.printStackTrace();
			}
		}
		if(exceptionsInARow <= 3)
		{
			//finalise modules when sim ends
			for(i=0; i<modules.length; i++)
			{
				modules[i].simEnd(this);
			}
		}


	}
	
	public void killAgent(AgentObject a) 
	{
		a.energy = 0;
		if(positionGrid[a.x][a.y]==a)
		{
			positionGrid[a.x][a.y] = null; //remove from potential collisions
		}
		//agents.remove(a);	
		toRemove.add(a); //add to list of dead to remove		
	}

	public long getTicks()
	{
		return this.ticks;
	}
	
	public float getTicksPerSecond()
	{
		return this.ticksPerSecond;
	}	
	public float getAvgTicksPerSecond()
	{
		long nanospertick = (System.nanoTime()-absoluteStartTime)/this.ticks; //nano seconds since start of sim/number of ticks
		if(nanospertick == 0) {nanospertick = 1; } //just in case, otherwise div by 0
		return 1.0f/(nanospertick/1000000000.0f); //1 divided by average seconds per tick = average ticks per second		
	}
	public long getRunTime() //returns seconds since start
	{
		return (System.nanoTime()-absoluteStartTime) / 1000000000;
	}

	
		
}
