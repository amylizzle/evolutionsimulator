package SimComponents;

import java.util.LinkedList;
import java.util.Random;
import java.io.Serializable;

import HelperFunctions.Node;


public class AgentObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6819599121262915208L;
	public long uniqueID;
	
	
	public int genus = 0;
	public int generation = 0;
	public double mutationrate = 0;
		
	private int lastbred = 0;
	public int dnahash = 0;
	public int chloroplasts = 0;
	public int shell = 0;
	
	public int x=0; 
	public int y=0;
	public int energy = 0;
	private int age=0;
	private long bred = 0;
	private long ate = 0;
	public String dna = "";
	public int dnapos = 0;
	private char state = 'X'; 
	public int idMarker = 0; //self filled, for identification of own species - no point using getters/setters for this
	private char rotation = 'N'; //N, E, S, W
	private int seeDepth = 5;
	
	private LinkedList<Integer> stack;
	private Node ancestry;
	
	public AgentObject()
	{
		this.x = 0;
		this.y = 0;
		this.dna = "";
		this.energy = 0;
		this.stack = new LinkedList<Integer>();
		this.dnahash = this.dna.hashCode();
		this.ancestry = new Node(this,null,null);
		Random random = new Random();
		uniqueID = random.nextLong();
		this.chloroplasts = 0;
		this.shell = 0;
	}
	
	public AgentObject(int x, int y, String dna, int energy, int genus, double mutationrate)
	{
		this.x = x;
		this.y = y;
		this.dna = dna;
		this.energy = energy;
		this.stack = new LinkedList<Integer>();
		this.dnahash = this.dna.hashCode();
		this.genus = genus;
		this.mutationrate = mutationrate;
		this.ancestry = new Node(this,null,null);
		Random random = new Random();
		uniqueID = random.nextLong();
		this.chloroplasts = 0;
		this.shell = 0;
	}
	
	@SuppressWarnings("unchecked")
	public AgentObject(AgentObject a)
	{
		this.age = a.age;
		this.ate = a.ate;
		this.bred = a.bred;
		this.dna = a.dna;
		this.dnahash = a.dnahash;
		this.dnapos = a.dnapos;
		this.energy = a.energy;
		this.genus = a.genus;
		this.lastbred = a.lastbred;
		this.rotation = a.rotation;
		this.seeDepth = a.seeDepth;
		this.stack = (LinkedList<Integer>) a.stack.clone();
		this.state = a.state;
		this.x = a.x;
		this.y = a.y;	
		this.mutationrate = a.mutationrate;
		this.generation = a.generation;
		this.ancestry = new Node(this,a.ancestry.leftParent,a.ancestry.rightParent);
		//this.ancestry = (Node) a.ancestry.clone(); //clone
		this.uniqueID = a.uniqueID;
		this.chloroplasts = a.getChloroplasts();
		this.shell = a.getShell();
	}
	
	public int getChloroplasts()
	{
		return this.chloroplasts;
	}
	
	public int getShell()
	{
		return this.shell;
	}
	
	public void setParent(AgentObject parent, AgentObject secondParent)
	{
		if(secondParent == null)
		{
			this.ancestry = new Node(this, parent.ancestry ,null);
		}
		else
		{
			this.ancestry = new Node(this, parent.ancestry , secondParent.ancestry);
		}
	}
	
	public Node getAncestry()
	{
		return this.ancestry;
	}
	
	public boolean canBreed()
	{
		return this.lastbred == 0; 
	}
	
	public void resetBreedDelay()
	{
		if(this.getState() != 'V')
		{
			this.lastbred = 50;
		}
		else
		{
			this.lastbred = 300;
		}
	}
	
	public char getState()
	{
		return this.state;
	}
	
	public void setState(char c)
	{
		this.state = c;
	}
	
	public char getRotation()
	{
		return this.rotation;
	}
	
	public void setRotation(char r)
	{
		if (r == 'N' ||r == 'E' ||r == 'S' ||r == 'W')
		{
			this.rotation = r;
		}
		
	}


	
	public void stackPush(int c)
	{
		this.stack.addFirst(c);
		if(this.stack.size()>10)
		{
			this.stack.removeLast();
		}
	}
	
	public int stackPop()
	{
		if(stack.size()>0)
		{
			int tmp = stack.getFirst();
			stack.removeFirst();
			return tmp;		
		}
		else
		{
			return 0;
		}
	}
	
	public int getSeeDepth()
	{
		return seeDepth;
	}
	
	public void incAge()
	{
	//	if(!this.ancestry.checkIntegrity()) //debug code
	//	{
	//		System.out.println("Ancestry failed integrity check!");
	//	}
		this.age++;
	
		if(this.lastbred > 0)
		{
			this.lastbred--;
		}
	}
	
	public int getAge()
	{
		return this.age;
	}
	
	public void recalcDNAHash()
	{
		this.dnahash = this.dna.hashCode();
	}
	
	public void jump(int jmploc)
	{
		if(jmploc < 0) //java's Math.abs() implementation is stupid. Math.abs(Integer.MIN_VALUE) = Integer.MIN_VALUE. Who thought that was a good idea?
		{
			if(jmploc == Integer.MIN_VALUE) 
				jmploc = Integer.MAX_VALUE;
			else
				jmploc = jmploc * -1;
		}

		this.dnapos = jmploc % this.dna.length();
	}
	
	public void incBred()
	{
		this.bred ++;
	}
	
	public void consumedNrg(int nrg)
	{
		this.ate += nrg;
		this.energy += nrg;
	}
	
	public long getBred()
	{
		return this.bred;
	}
	
	public long getAte()
	{
		return this.ate;
	}
	
	public void setSight(int dist)
	{
		dist = dist % 256;
		if(dist < 0) //java's Math.abs() implementation is stupid. Math.abs(Integer.MIN_VALUE) = Integer.MIN_VALUE. Who thought that was a good idea?
		{
			if(dist == Integer.MIN_VALUE) 
				dist = Integer.MAX_VALUE;
			else
				dist = dist * -1;
		}
		this.seeDepth = dist;
	}
	
	public String toString()
	{
		String result = super.toString()+":{\n";
		
		result += "\tUID: "+uniqueID;
		result += "\n\tidMarker: "+idMarker;
		result += "\n\tx: "+x;
		result += "\n\ty: "+y;
		result += "\n\tenergy: "+energy;
		result += "\n\tshell: "+shell;
		result += "\n\tchloroplasts: "+chloroplasts;
		result += "\n\tdna: "+dna;
		result += "\n\tdnalength: "+dna.length();
		result += "\n\tdnapos: "+dnapos;
		result += "\n\trotation: "+rotation;
		result += "\n\tstate: "+state;
		result += "\n\tstack: "+stack;
		result += "\n\tage: "+age;
		result += "\n\tate: "+ate;
		result += "\n\toffspring: "+bred;
		result += "\n\tsight: "+seeDepth;
		result += "\n\tgenus:" +genus;
		result += "\n\tgeneration:" +generation;
		result += "\n\tmutationrate:" +mutationrate;
	//	result += "\n\tancestry:" +ancestry; //breaks things - basically causes a full dump of the ancestry tree of every related agent. Not good.
		result +="\n}";
		return result;
	}
}
