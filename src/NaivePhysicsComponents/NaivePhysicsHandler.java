package NaivePhysicsComponents;

import java.util.Random;

import SimComponents.AgentObject;
import SimComponents.PhysicsHandler;
import SimComponents.SimObject;

public class NaivePhysicsHandler extends PhysicsHandler {

	//returns the agent which loses in the fight: <0 is a, >0 is b, =0 is neither
	private int getLoser(AgentObject a, AgentObject b)
	{
		int apoints = (a.getShell() * 10) - (a.getChloroplasts() * 5);
		if(a.getState() == 'E')
		{
			apoints += a.energy*5;
		}
		int bpoints = (b.getShell() * 10) - (b.getChloroplasts() * 5);
		if(b.getState() == 'E')
		{
			bpoints += b.energy*5;
		}
		return bpoints - apoints;
	}
	
	@Override
	public void doMovement(char dir, AgentObject a, SimObject s)
	{
		if(a.getChloroplasts() == 0) //can only move if you're a veg
		{
			int oldx = a.x;
			int oldy = a.y;
			int newy = oldy;
			int newx = oldx;

			switch(dir)
			{
			case 'N':
				newy--; break;
			case 'E':
				newx++; break;
			case 'S':
				newy++; break;
			case 'W':
				newx--; break;	
			default: break;
			}

			//if you're going to move outside sim boundaries, don't move at all.
			if ((newx >= s.width) || (newx <= 0))
				newx = oldx;

			if ((newy >= s.height) || (newy <= 0))
				newy = oldy;

			//	newx = (s.width+newx) % s.width;
			//	newy = (s.height+newy) % s.height;

			if(doCollision(a,s.positionGrid[newx][newy],s))
			{
				s.positionGrid[oldx][oldy] = null;
				s.positionGrid[newx][newy] = a;
				a.x = newx;
				a.y = newy;
				//a.energy = a.energy-1;
			}

		}
	}

	@Override
	public void breedAgents(AgentObject a, AgentObject b, SimObject s)
	{

		Random myrandom = new Random();
		if(b == null)
		{
			b = new AgentObject(); //create a placeholder empty agent with no dna & no energy
		}

		//was 200
		int energyBarrier = 200;
		
		if((a.energy+b.energy > energyBarrier) && (a.canBreed() && b.canBreed()))
		{			
			int newx = (a.x);//+myrandom.nextInt(4)-2);
			int newy = (a.y);//+myrandom.nextInt(4)-2);
			
			switch(a.getRotation())
			{
			case 'N': newy+=2; break;
			case 'E': newx+=2; break;
			case 'S': newy-=2; break;
			case 'W': newx-=2; break;
			default:
				newx += (myrandom.nextInt(4)-2);
				newy += (myrandom.nextInt(4)-2);
				break;
			}
			
			boolean placed = false;
			int tries = 0;
			while((!placed) && (tries < 10)) //look for a place to put the new agent
			{		

				if ((newx >= s.width) || (newx <= 0))
					newx = (a.x+myrandom.nextInt(4)-2);

				if ((newy >= s.height) || (newy <= 0))
					newy = (a.y+myrandom.nextInt(4)-2);

				if((newx >= 0) && (newx <= s.width) && (newy >=0) && (newy <=s.height))
					if(s.positionGrid[newx][newy] == null)
					{
						a.incBred();
						a.resetBreedDelay();
						b.resetBreedDelay();
						AgentObject c = new AgentObject();
						c.energy = a.energy / 2;
						c.energy += b.energy / 2;
						a.energy = a.energy / 2;
						b.energy = b.energy /2;
						
						c.shell = a.shell / 2;
						c.shell += b.shell / 2;
						a.shell = a.shell / 2;
						b.shell = b.shell /2;
						
						c.chloroplasts = a.chloroplasts / 2;
						c.chloroplasts += b.chloroplasts / 2;
						a.chloroplasts = a.chloroplasts / 2;
						b.chloroplasts = b.chloroplasts /2;
						if(b.mutationrate == 0)
						{
							c.mutationrate = a.mutationrate;
						}
						else
						{
							c.mutationrate = (a.mutationrate+b.mutationrate)/2;
						}

						int crossoverpoint = myrandom.nextInt(Math.min(a.dna.length(), b.dna.length())+1);
						if(a.dna.length() < b.dna.length())
						{
							c.dna = a.dna.substring(0,crossoverpoint) + b.dna.substring(crossoverpoint,b.dna.length());
						}
						else
						{
							c.dna = b.dna.substring(0,crossoverpoint) + a.dna.substring(crossoverpoint,a.dna.length());
						}

						c.x = newx;
						c.y = newy;
						
						if(b.genus != 0)
						{
							c.genus = (a.genus+b.genus)/2;
						}
						else
						{
							c.genus = a.genus;
						}


						if(b.generation > a.generation)
						{
							c.generation = b.generation+1;
						}
						else
						{
							c.generation = a.generation+1;
						}

						
						if(b.mutationrate == 0)
						{
							b = null; //make sure we don't put placeholder agents into the ancestry tree
						}

						c.setParent(a, b);

						s.agents.add(c);
						s.positionGrid[newx][newy] = c;
						placed = true;

					}

				tries++;
				if(newx < a.x)
					newx--;
				else if(newx > a.x)
					newx++;
				else if(newy < a.y)
					newy--;
				else
					newy++;	
			}//while(!placed)
		}//if can breed
	}

	@Override
	public boolean doCollision(AgentObject a, AgentObject b, SimObject s) //returns boolean reflecting whether or not the move is possible
	{
		if(a == b) { return false; } //can't collide with yourself!
		if(b == null) { return true; } //no agent there, go right ahead

		//bumped into an agent, use states to decide what to do
		switch(a.getState()) //no need for breaks - return statements make them unreachable
		{
			case 'M': //either get eaten, breed, or do nothing
				switch(b.getState())
				{
					case 'E': 
						if(getLoser(a,b) > 0) //b eats a
						{
							b.consumedNrg(a.energy);
							s.killAgent(a);
							return false;
						}
						else 
						{ //b can't eat a, a isn't trying to eat, just bump
							return false;
						}						
					case 'M': //let's fuck!
						breedAgents(a,b,s);
						return false;
					default: //can't move there
						return false;
				}
			case 'E': //either eat or get eaten - in the event of stalemate, no move
				if(getLoser(a,b) > 0) //b beats a
				{
					//is b going to eat?
					if(b.getState() == 'E')
					{
						b.consumedNrg(a.energy);
						s.killAgent(a);
						return false;
					}
					else //you didn't get eaten, but you can't eat this one - no move
					{
						return false;
					}
				}
				else if(getLoser(a,b) < 0) //a beats b
				{
					//a is already in eat state, so omnomnom time
					a.consumedNrg(b.energy);
					s.killAgent(b);
					return true;
				}
				else //stalemate
				{
					return false;
				}
			default: //your state does not allow you to move into an occupied cell
				return false;
		}
	}
}
