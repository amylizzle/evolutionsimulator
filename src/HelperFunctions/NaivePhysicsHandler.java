package HelperFunctions;

import java.util.Random;

import SimComponents.AgentObject;
import SimComponents.PhysicsHandler;
import SimComponents.SimObject;

public class NaivePhysicsHandler extends PhysicsHandler {

	@Override
	public void doMovement(char dir, AgentObject a, SimObject s)
	{
		if(a.getState() != 'V') 
		{
			int oldx = a.x;
			int oldy = a.y;
			int newy = oldy;
			int newx = oldx;

			switch(dir)
			{
			case 'N':
				newy--; break;
			case 'E':
				newx++; break;
			case 'S':
				newy++; break;
			case 'W':
				newx--; break;	
			default: break;
			}

			//if you're going to move outside sim boundaries, don't move at all.
			if ((newx >= s.width) || (newx <= 0))
				newx = oldx;

			if ((newy >= s.height) || (newy <= 0))
				newy = oldy;

			//	newx = (s.width+newx) % s.width;
			//	newy = (s.height+newy) % s.height;

			if(doCollision(a,s.positionGrid[newx][newy],s))
			{
				s.positionGrid[oldx][oldy] = null;
				s.positionGrid[newx][newy] = a;
				a.x = newx;
				a.y = newy;
				//a.energy = a.energy-1;
			}

		}
	}

	@Override
	public void breedAgents(AgentObject a, AgentObject b, SimObject s)
	{

		Random myrandom = new Random();
		if(b == null)
		{
			b = new AgentObject(); //create a placeholder empty agent with no dna & no energy
		}

		//was 200
		int energyBarrier = 200;
		if(a.getState() == 'V' || b.getState()=='V')
		{
			energyBarrier = 1000;
		}
		if((a.energy+b.energy > energyBarrier) && (a.canBreed() && b.canBreed()))
		{			
			int newx = (a.x+myrandom.nextInt(4)-2);
			int newy = (a.y+myrandom.nextInt(4)-2);

			boolean placed = false;
			int tries = 0;
			while((!placed) && (tries < 10)) //look for a place to put the new agent
			{
				if(newx < a.x)
					newx--;
				else
					newx++;

				if(newy < a.y)
					newy--;
				else
					newy++;	

				if ((newx >= s.width) || (newx <= 0))
					newx = (a.x+myrandom.nextInt(4)-2);

				if ((newy >= s.height) || (newy <= 0))
					newy = (a.y+myrandom.nextInt(4)-2);

				if((newx >= 0) && (newx <= s.width) && (newy >=0) && (newy <=s.height))
					if(s.positionGrid[newx][newy] == null)
					{
						a.incBred();
						a.resetBreedDelay();
						b.resetBreedDelay();
						AgentObject c = new AgentObject();
						c.energy = a.energy / 2;
						c.energy += b.energy / 2;
						//if(c.energy > (a.energy + b.energy)/2) { System.out.println("D:"); }
						a.energy = a.energy / 2;
						b.energy = b.energy /2;
						//if(c.energy > (a.energy + b.energy)) { System.out.println("D:"); }
						if(b.mutationrate == 0)
						{
							c.mutationrate = a.mutationrate;
						}
						else
						{
							c.mutationrate = (a.mutationrate+b.mutationrate)/2;
						}

						int crossoverpoint = myrandom.nextInt(Math.min(a.dna.length(), b.dna.length())+1);
						if(a.dna.length() < b.dna.length())
						{
							c.dna = a.dna.substring(0,crossoverpoint) + b.dna.substring(crossoverpoint,b.dna.length());
						}
						else
						{
							c.dna = b.dna.substring(0,crossoverpoint) + a.dna.substring(crossoverpoint,a.dna.length());
						}

						c.x = newx;
						c.y = newy;
						if(b.genus != 0)
						{
							c.genus = (a.genus+b.genus)/2;
						}
						else
						{
							c.genus = a.genus;
						}


						if(b.generation > a.generation)
						{
							c.generation = b.generation+1;
						}
						else
						{
							c.generation = a.generation+1;
						}

						if(b.mutationrate == 0)
						{
							b = null; //make sure we don't put placeholder agents into the ancestry tree
						}

						c.setParent(a, b);

						s.agents.add(c);
						s.positionGrid[newx][newy] = c;
						placed = true;

					}

				tries++;
			}//while(!placed)
		}//if can breed
	}

	@Override
	public boolean doCollision(AgentObject a, AgentObject b, SimObject s) //returns boolean reflecting whether or not the move is possible
	{
		if(a == b) { return false; } //can't collide with yourself!
		if(b == null) { return true; } //no agent there, go right ahead

		if((a.getState()=='M') && (b.getState()=='M')) //both ready to mate
		{
			//mate
			breedAgents(a,b,s);
			return false;
		}
		else
			if((a.getState()=='E') && (b.getState()=='E'))
			{
				if(a.energy>=b.energy) //a stronger than b, a eats b
				{
					a.consumedNrg(b.energy);
					s.killAgent(b);		
					return true;
				}
				else  //b stronger than a, b eats a
				{
					b.consumedNrg(a.energy);
					s.killAgent(a);
					return false;
				}
			} 
			else if(a.getState()=='E')
			{
				a.consumedNrg(b.energy);
				s.killAgent(b);
				return true;
			}	
			else if(b.getState()=='E')
			{
				b.consumedNrg(a.energy);
				s.killAgent(a);
				return false;
			} 
			else //both aren't in mate state, neither in eat state
			{
				return false;
			}
	}
}
