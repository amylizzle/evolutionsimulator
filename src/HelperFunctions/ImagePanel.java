package HelperFunctions;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class ImagePanel extends JPanel{

	private static final long serialVersionUID = 8559702878784618894L;
	private BufferedImage image;

    public ImagePanel()
    {
    	this.image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    }
    
    public ImagePanel(BufferedImage image) {
    	super();
    	this.image = image;
    }

    public void setImage(BufferedImage image)
    {
    	this.image = image;
    	this.setPreferredSize(new Dimension(image.getWidth(),image.getHeight()));
    	this.repaint();
    }
    
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);             
    }

}
