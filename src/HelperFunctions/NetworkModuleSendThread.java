package HelperFunctions;
import java.io.ObjectOutputStream;
import java.util.concurrent.ConcurrentLinkedQueue;

import SimComponents.AgentObject;


public class NetworkModuleSendThread extends Thread {
	public ConcurrentLinkedQueue<AgentObject> agentsToSend = new ConcurrentLinkedQueue<AgentObject>();
	private ObjectOutputStream cos;
	public volatile boolean died = false; //like interupted(), except it actually works.
	
	public void initialise(ObjectOutputStream oos)
	{
		cos = oos;
	}
	
	public synchronized void killThread()
	{
		this.died = true;
		this.interrupt();
	}
	
	public void run()
	{
		while(!(this.isInterrupted() || this.died))
		{
			AgentObject a = agentsToSend.poll();
			if(a != null)
			{
				try {
					if(a.energy != 0)
					{
						cos.writeObject(a);
						cos.flush();
					}
				}	
				catch (Exception e) {
					this.killThread();
					e.printStackTrace();
				}
			}
		}
	}
}
