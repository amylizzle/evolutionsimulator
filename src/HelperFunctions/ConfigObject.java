package HelperFunctions;

import java.util.Hashtable;

public class ConfigObject {

	private String modName;
	private Hashtable<String,String> keyValues;
	
	public ConfigObject(String moduleName)
	{
		modName = moduleName;
		keyValues = new Hashtable<String,String>();
		keyValues.put("enabled", "true"); //default
	}
	
	public String getName()
	{
		return modName;
	}
	
	public String getValue(String key)
	{
		return keyValues.get(key);
	}
	
	public void parseLine(String line) throws IllegalArgumentException
	{
		String[] pair = line.split("=");
		if(pair.length != 2)
		{
			throw new IllegalArgumentException();
		}
		System.out.println(this.getName()+":"+pair[0]+" == "+pair[1]); //debug line
		keyValues.put(pair[0], pair[1]);
	}
}
