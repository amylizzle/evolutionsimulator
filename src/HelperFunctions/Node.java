package HelperFunctions;

import java.io.Serializable;

import SimComponents.AgentObject;


public class Node implements Serializable { 

	private static final long serialVersionUID = -4540232526872961239L;
	private AgentObject value = null;
	public Node leftParent = null;
	public Node rightParent = null;

	public Node(AgentObject value, Node left, Node right) {
		this.value = value;
		this.leftParent = left;
		this.rightParent = right;
	}
	
	public AgentObject getValue()
	{
		return this.value;
	}
	
	public void setValue(AgentObject a)
	{
		this.value = a;
	}
	
	public boolean checkIntegrity()
	{
		if(this.value.mutationrate == 0)
		{
			return false;
		}
		boolean result = true;
		if(this.leftParent != null)
		{
			result = result && this.leftParent.checkIntegrity();			
		}
		if(this.rightParent != null)
		{
			result = result && this.rightParent.checkIntegrity();
		}
		return result;
	}
	
	public void prune()
	{

		if(this.leftParent != null)
		{
			this.leftParent.prune();
			this.leftParent = null;
		}
		if(this.rightParent != null)
		{
			this.rightParent.prune();
			this.rightParent = null;
		}
	}
	
}


	



