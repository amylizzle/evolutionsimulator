package HelperFunctions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Hashtable;

import SimComponents.Module;

public class ModuleLoader {

	public static Hashtable<String,ConfigObject> loadConfigs(String configPath)
	{
		Hashtable<String,ConfigObject> result = new Hashtable<String,ConfigObject>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(configPath));
			String line = "";
			ConfigObject co = null;
			while((line = br.readLine()) != null)
			{
				if(line.length() > 0)
				{
					if(line.charAt(0) == '[')
					{
						if(co != null)
						{
							result.put(co.getName(), co);
						}
						co = new ConfigObject(line.substring(1,line.length()-1));
					}
					else
					{
						if(co != null)
						{
							co.parseLine(line);
						}
					}
				}
			}
			//end of file, add last configobject to hashtable
			if(co != null)
				result.put(co.getName(), co);
			br.close();
			return result;
		} catch (Exception e) {
			System.out.println("Error reading config file! "+e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Class<Module>[] loadModules(String directoryPath)
	{
		try
		{
			File dir = new File(directoryPath);
			File [] files = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File blah, String name) {
					return (name.endsWith(".class") && !name.contains("$"));
				}
			});
			if(files == null)
			{
				System.out.println("The provided directory is not valid: "+directoryPath+" == "+dir);
				return new Class[0];
			}
			Class<Module>[] result = new Class[files.length];
		
			URL classUrl = new URL("file:///"+directoryPath);
    		URL[] classUrls = { classUrl };
    		@SuppressWarnings("resource")
			URLClassLoader ucl = new URLClassLoader(classUrls); //this resource isn't closed because JRE 1.6 does not
    															//support URLClassLoader.close()
    	
			for (int i=0; i<files.length; i++) {
				String tmpname = "Modules."+files[i].getName().substring(0,files[i].getName().indexOf(".class"));
				Class<Module> c = (Class<Module>) ucl.loadClass(tmpname); 
	    		   		
	    		result[i] = c;
			}
			//ucl.close(); //not jre6 compatible
			return result;
		}
		catch(Exception e)
		{
			
			System.out.println("MODULE LOADING FAILED WITH ERROR:");
			e.printStackTrace();
			return new Class[0];			
		}
	}
	

	
}
